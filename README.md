Snipcart
========

This module aims to integrate Snipcart with Drupal Commerce.

It exposes a Drupal REST resource endpoint in order to implements
Snipcart compatible data-uri.
